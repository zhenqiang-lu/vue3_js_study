'use strict'
const path = require('path')
//const UglifyJsPlugin = require('uglifyjs-webpack-plugin') // 去掉注释
//const CompressionWebpackPlugin = require('compression-webpack-plugin') // 开启压缩
const AutoImport = require('unplugin-auto-import/webpack')
const Components = require('unplugin-vue-components/webpack')
const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')

const isProduct = process.env.NODE_ENV === 'production'
const env = process.env

function resolve(dir) {
   return path.join(__dirname, dir)
}

const name = 'vue-cli'

const port = process.env.port || 9999



// module.exports = {
//    // ...
//    plugins: [
//       AutoImport({
//          resolvers: [ElementPlusResolver()],
//       }),
//       Components({
//          resolvers: [ElementPlusResolver()],
//       }),
//    ],
// }


// module.exports = {
//    plugins: {
//       'autoprefixer': {
//          browsers: ['Android >= 4.0', 'iOS >= 7']
//       },
//       'postcss-pxtorem': {
//          rootValue: 16,//结果为：设计稿元素尺寸/16，比如元素宽320px,最终页面会换算成 20rem
//          propList: ['*']
//       }
//    }
// }




module.exports = {
   // 部署生产环境和开发环境下的URL
   publicPath: '/community-Q-A',

   // 在npm run build 或 yarn build 时 ，生成文件的目录名称（要和baseUrl的生产环境路径一致）（默认dist）
   outputDir: 'community-Q-A',
   // 用于放置生成的静态资源 (js、css、img、fonts) 的；（项目打包之后，静态资源会放在这个文件夹下）
   assetsDir: 'static',
   // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建
   productionSourceMap: false,
   // 取消严格模式
   lintOnSave: false,
   // webpack-dev-server 相关设置
   devServer: {
      host: '0.0.0.0',
      port,
      open: true,
      proxy: {
         '/zhonger': {
            target: `https://ai.insurtechbank.net/`,
            changeOrigin: true
         }
      },
      disableHostCheck: true
   },
   // 基本配置
   configureWebpack: (config) => {
      if (isProduct) {
         config.plugins.push(
            new UglifyJsPlugin({
               uglifyOptions: {
                  output: {
                     comments: false // 去掉注释
                  },
                  warnings: false,
                  compress: {
                     drop_console: false,
                     drop_debugger: true
                     // pure_funcs: ['console.log'] // 移除console
                  }
               }
            })
         )

         config.plugins.push(
            new CompressionWebpackPlugin({
               algorithm: 'gzip',
               test: /\.(js|css|json|txt|html|ico|svg)(\?.*)?$/i, // 匹配文件名
               threshold: 10240, // 对超过10k的数据压缩
               deleteOriginalAssets: false, // 不删除源文件
               minRatio: 0.8 // 压缩比
            })
         )
      }
   },
   // 高级配置
   chainWebpack(config) {
      config.name = name
      // 添加别名
      config.resolve.alias.set('@', resolve('src'))
      // 移除 preload 插件
      config.plugins.delete('preload')
      // 移除 prefetch 插件
      config.plugins.delete('prefetch')
      config.optimization.delete('splitChunks')
      config.module
         .rule('svg')
         .exclude.add(resolve('src/assets/'))
         .end()
      config.module
         .rule('icons')
         .test(/\.svg$/)
         .include.add(resolve('src/assets/'))
         .end()
         .use('svg-sprite-loader')
         .loader('svg-sprite-loader')
         .options({
            symbolId: 'icon-[name]'
         })
         .end()

      config.when(isProduct, (config) => {
         // 开启js分离
         config.optimization.splitChunks({
            chunks: 'async',
            minSize: 1024 * 10, // 30000,
            maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 6,
            maxInitialRequests: 4,
            automaticNameDelimiter: '~',
            cacheGroups: {
               commons: {
                  name: 'chunk-commons',
                  test: resolve('src/components'), // 可以为字符串，正则表达式，函数，以module为维度进行抽取，只要是满足条件的module都会被抽取到该common的chunk中，为函数时第一个参数是遍历到的每一个模块，第二个参数是每一个引用到该模块的chunks数组。
                  minChunks: 3, // 最少被几个chunk引用
                  priority: 5, // 优先级，一个chunk很可能满足多个缓存组，会被抽取到优先级高的缓存组中
                  reuseExistingChunk: true //  如果该chunk中引用了已经被抽取的chunk，直接引用该chunk，不会重复打包代码
               },
               vendors: {
                  name: 'chunk-vendors',
                  test: /[\\/]node_modules[\\/]/,
                  priority: -10,
                  chunks: 'initial'
               },
               vant: {
                  name: 'vant',
                  test: /[\\/]node_modules[\\/]vant[\\/]/,
                  minSize: 0,
                  minChunks: 1,
                  reuseExistingChunk: true,
                  chunks: 'all'
               }
            }
         })
         config.optimization.runtimeChunk('single')
      })
   },
   css: {
      loaderOptions: {
         postcss: {
            plugins: [
               require('postcss-pxtorem')({ // 把px单位换算成rem单位
                  rootValue: 37.5, // 换算的基数(设计图750的根字体为75)
                  // selectorBlackList: ['weui', 'mu'], // 忽略转换正则匹配项
                  propList: ['*']//* 代表将项目中的全部进行转换，单个转换如width、height等
               })
            ]
         }
      }
   }
}

