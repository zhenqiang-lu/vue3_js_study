
import { createApp } from 'vue'
import App from '../App.vue'
const app = createApp(App)
function registerComponents(moduleContext) {
  // https://webpack.js.org/guides/dependency-management/
  moduleContext.keys().forEach(key => {
    const module = moduleContext(key)
    const component = module.default
    const keyParts = key.replace(/\.vue$/, '').split('/')
    const componentName = keyParts[keyParts.length - 1]
    app.component(componentName, component)
  })
}

registerComponents(require.context('.', true, /\.vue$/))
