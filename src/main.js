import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'vant/lib/index.css'
import { Icon } from 'vant'
import store from './store'
//import VConsole from 'vconsole'
// 组件注册
import './components'
const app = createApp(App)
app.use(Icon)
// 移动端rem适配
import 'amfe-flexible/index'
// 全局样式
import './style/index.scss'
const env = process.env
// if (env.VUE_APP_MODE !== 'online') {
//    new VConsole()
// }


createApp(App).use(store).use(router).mount('#app')












// import Vue from 'vue'
// import App from './App.vue'
// import router from '@/router'
// import 'vant/lib/index.css'
// import { Icon } from 'vant'

// Vue.use(Icon)
// import VConsole from 'vconsole'

// // 自定义 Icon
// // import './assets/icon/iconfont.css'

// // 组件注册
// import './components'

// // 移动端rem适配
// import 'amfe-flexible/index'
// // 全局样式
// import './style/index.scss'
// const env = process.env
// if (env.VUE_APP_MODE !== 'online') {
//   new VConsole()
// }

// Vue.config.productionTip = false

// new Vue({
//   router,
//   render: h => h(App)
// }).$mount('#app')
