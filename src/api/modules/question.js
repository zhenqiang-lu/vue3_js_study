import request from '@/api/config'
function urlHandle(method, url, params) {
  return request[method](`${url}`, params)
}
/**
 * 答题提交
 * @param {Object} params 参数对象
 */

const subOrder = params => {
  return request.post('/zhonger/freecenter/v3/oto/subOrder', params)
}
/**
 * 暂存手机号
 * @param {Object} params 参数对象
 */
const subAppoint = params => {
  return request.post('/zhonger/freecenter/v3/oto/subAppoint', params)
}

/**
 * 验证手机号是否参加过活动
 * @param {Object} params 参数对象
 */
const checkMobileOrder = params => {
  return request.post('/zhonger/freecenter/v3/oto/checkMobileOrder', params)
}

/**
 * 获取验证码
 * @param {Object} params 参数对象
 */
const sendCheckCode = params => {
  return urlHandle('get', `/zhonger/freecenter/car/api/v3/sms/sendCheckCode?version=1.5&mobile=` + params)
}

/**
 * 验证验证码
 * @param {Object} params 参数对象
 */
const validCheckCode = params => {
  return urlHandle('get', `/zhonger/freecenter/car/api/v3/sms/validCheckCode?version=1.5&mobile=` + params.mobile + `&checkCode=` + params.checkCode)
}

/**
 * 新增园区
 * @param {Object} params 参数对象
 */
const getParkAreaInfo = params => {
  return urlHandle('get', `/zhonger/freecenter/v3/oto/getParkAreaInfo/` + params)
}

/**
 * 埋点
 * @param {Object} params 参数对象
 */
const buriedPoint = params => {
  return urlHandle('post', `/zhonger/freecenter/v3/oto/buriedPoint`, params)
}

export default {
  subOrder,
  sendCheckCode,
  validCheckCode,
  checkMobileOrder,
  subAppoint,
  getParkAreaInfo,
  buriedPoint
}
