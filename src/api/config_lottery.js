import axios from 'axios'
import { getToken } from '@/api/auth'
// 全局引入vant的提示框
import { Toast } from 'vant'
const visitorId = window.localStorage.getItem('visitorId')
/**
 * axios实例
 */
const service = axios.create({
  baseURL: process.env.VUE_APP_LOTTERY_API,
  timeout: 60000
})

/**
 * 请求拦截器配置
 */
service.interceptors.request.use(
  config => {
    /**
     * 请求头信息配置
     */
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
    config.headers['Accept-Language'] = 'zh-CN'
    if (getToken()) {
      config.headers['token'] = getToken()
    }
    if (visitorId) {
      config.headers['visitorId'] = visitorId
    }
    if (config.method === 'post') {
      if (!config.data) {
        config.data = {}
      }
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

/**
 * 响应拦截器
 */
service.interceptors.response.use(
  response => {
    /**
     * 根据接口响应数据结构做出对应处理
     */
    Toast.clear()
    const res = response.data
    if (res.ok) {
      return Promise.resolve(res)
    } else {
      // eslint-disable-next-line eqeqeq
      if (res.code == '9001' || res.code == '9002' || res.code == '9003') {
        window.webkit && window.webkit.messageHandlers.loginExpired.postMessage(res.msg)
        // eslint-disable-next-line no-undef
        app.loginExpired && app.loginExpired(res.msg)
        return Promise.resolve(res)
      } else {
        return Promise.resolve(res)
      }
    }
  },
  error => {
    Toast.clear()
    return Promise.reject(error)
  }
)

export default service
