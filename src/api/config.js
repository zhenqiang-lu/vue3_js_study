import axios from 'axios'
import { getToken } from '@/api/auth'
// 全局引入vant的提示框
import { Toast } from 'vant'
import CancelRequest from './cancelRequest.js'

const cancelRequest = new CancelRequest()

/**
 * axios实例
 */
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 60000
})

/**
 * 请求拦截器配置
 */
service.interceptors.request.use(
  config => {
    /**
     * 请求头信息配置
     */
    config.headers['Content-Type'] = 'application/json'
    config.headers['Accept-Language'] = 'zh-CN'
    if (getToken()) {
      config.headers['token'] = getToken()
    }
    if (config.method === 'post') {
      if (!config.data) {
        config.data = {}
      }
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

/**
 * 响应拦截器
 */
service.interceptors.response.use(
  response => {
    // 移除成功请求记录
    cancelRequest.removeRequestKey(response.config)

    /**
     * 根据接口响应数据结构做出对应处理
     */
    Toast.clear()
    const res = response.data

    return Promise.resolve(res)
  },
  error => {
    // 移除失败的请求记录
    cancelRequest.removeRequestKey(error.config || {})
    if (axios.isCancel(error)) {
      // 取消请求后，去除错误处理
      return new Promise(() => { })
    }

    Toast.clear()
    return Promise.reject(error)
  }
)

export default service
