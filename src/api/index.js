/**
 * @author
 * @description 项目中用到的API维护到这里，写好传参等注释说明，接口通用的处理，在这里处理
 */

// 问答接口
import question from './modules/question'

export default {
  question
}
